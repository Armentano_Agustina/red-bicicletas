var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function() {

    beforeAll(async() => {
        await mongoose.disconnect();
    });

    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false, useCreateIndex: true })
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('we are connected to test database');
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            mongoose.disconnect(err);
            done();
        });
    });
    describe('createInstance', () => {
        it('se crea una instancia'), () => {
            var bici = Bicicleta.createInstance("violeta", "urbana", [-31.352241, -64.184132]);
            expect(bici.color).toBe("violeta");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(-31.352241);
            expect(bici.ubicacion[1]).toEqual(-64.184132);

        }
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', () => {
            Bicicleta.allBicis(function(err, bicis) {
                if (err) console.log(err)
                expect(bicis.length).toBe(0);

            });
        });
    });
    describe('Bicicleta.add', () => {
        it('se agrega una sola bicicleta', (done) => {
            var bici = new Bicicleta({ _id: this._id, color: "azul", modelo: "montaña", ubicacion: [-31.352241, -64.184132] });
            Bicicleta.add(bici, function(err, newBici) {
                if (err) console.log(err)
                Bicicleta.allBicis(function(err, bicicletas) {
                    expect(bicicletas.length).toEqual(1);
                    expect(bicicletas[0].color).toBe("azul");
                    done()
                })
            })
        })
    });
    //lo pruebo porque lo pide en el video pero en realidad yo preferi hacerlo con id
    describe('Bicicleta.findbyCode', () => {
        it('se busca un elemento por su ""codigo""', (done) => {
            Bicicleta.allBicis(function(err, bicicletas) {
                expect(bicicletas.length).toBe(0);
                var biciNueva = new Bicicleta({ color: "amarillo", modelo: "urbana", ubicacion: [-31.352241, -64.184132] })
                id = biciNueva._id
                Bicicleta.add(biciNueva, function(err, bici_Nueva) {
                    if (err) console.log(err)
                    var biciNueva2 = new Bicicleta({ color: "amarillopatito", modelo: "urbana", ubicacion: [-31.352241, -64.184132] })
                    Bicicleta.add(biciNueva2, function(err, newbici2) {
                        if (err) console.log(err)
                        Bicicleta.findByCode(id, function(err, biciEncontrada) {
                            if (err) console.log(err)
                            expect(biciEncontrada.color).toBe('amarillo')
                            expect(biciEncontrada.modelo).toBe('urbana')
                            done()
                        })

                    })
                })
            })
        })
    });

});
/*describe('',()=>{
        it('',()=>{
            
        })
    })*/