var mongoose = require('mongoose');
var Usuario = require('../../models/usuario');
var Bicicleta = require('../../models/bicicleta');
var Reserva = require('../../models/reserva');

describe('Testing Usuario', () => {
    beforeAll(async() => {
        await mongoose.disconnect();
    });

    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false, useCreateIndex: true })
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('we are connected to test database');
            done();
        });
    });
    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            mongoose.disconnect(err);
            done();
        });
    });

    describe('reserva', () => {
        it('se crea una reserva', (done) => {
            var user = new Usuario({ nombre: "agus" });
            Usuario.create(user, function(err, data) {
                if (err) console.log(err)
                console.log('usuario creado');
            })
            var iduser = user._id;
            var bici = new Bicicleta({ _id: this._id, color: "azul", modelo: "montaña", ubicacion: [-31.352241, -64.184132] });
            Bicicleta.add(bici)
            var idbici = bici._id
            var reserva = new Reserva({ desde: 2020 - 09 - 20, hasta: 2020 - 09 - 25, Bicicleta: idbici, Usuario: iduser });
            Reserva.create(reserva, function(err, data) {
                if (err) console.log(err)
                console.log('reserva creada');
                done()
            })

        })
    });
})