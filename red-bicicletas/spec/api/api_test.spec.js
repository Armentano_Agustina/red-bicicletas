var Bicicleta = require('../../models/bicicleta');
var request = require('request')
var mongoose = require('mongoose');
const bicicleta = require('../../models/bicicleta');
require('../../bin/www')


describe('Bicicleta API', () => {
    beforeAll(async() => {
        await mongoose.disconnect();
    });

    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false, useCreateIndex: true })
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('we are connected to test database');
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            mongoose.disconnect(err);
            done();
        });
    });
    describe('/bicicleta_list /', () => {
        it('status 200', (done) => {

            var bici_1 = new Bicicleta({ _id: this._id, color: "azul", modelo: "montaña", ubicacion: [-31.352241, -64.184132] });
            Bicicleta.add(bici_1);

            request.get('http://localhost:3000/api/bicicletas/', function(error, response, body) {
                expect(response.statusCode).toBe(200);
                done()
            })

        })
    });
    describe('/POST BICICLETAS /create', () => {
        it('status 200', (done) => {
            var headers = { 'Content-Type': 'application/json' };
            request.post({
                    headers: headers,
                    url: 'http://localhost:3000/api/bicicletas/create',
                    body: '{"color": "rojo", "modelo": "urbana", "ubicacion": [-31.352241, -64.184132] }'
                },
                function(err, response) {
                    expect(response.statusCode).toBe(200);

                }, request.get('http://localhost:3000/api/bicicletas/', function(error, response, body) {
                    expect(response.statusCode).toBe(200);
                    done()
                })

            )
        })
    });
    describe('/DELETE BICICLETAS /delete', () => {
        it('status 200', (done) => {
            var headers = { 'Content-Type': 'application/json' };
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: '{"color": "morado", "modelo": "urbana", "ubicacion": [-31.352241, -64.184132] }'
            }), request.delete({
                    headers: headers,
                    url: 'http://localhost:3000/api/bicicletas/delete',
                    body: Bicicleta.findOne({})._id
                }, function(err, response, body) {
                    expect(response.statusCode).toBe(200);
                    done();

                }

            )
        })
    });

});

/*describe('/POST BICICLETAS /create', () => {
        it('status 200', (done) => {
            var headers = { 'Content-Type': 'application/json' };
            var bici = '{ "id": 10, "color": "rojo", "modelo": "urbana", "lat":-25251465,"lng":-48756568964}';
            request.post({
                    headers: headers,
                    url: 'http://localhost:5000/api/bicicletas/create',
                    body: bici
                }, function(err, response, body) {
                    expect(response.statusCode).toBe(200);
                    expect(Bicicleta.findById(10).color).toBe('rojo');
                    expect(Bicicleta.findById(10).modelo).toBe('urbana');

                    done();

                }

            )
        })
    })

    describe('/DELETE BICICLETAS /delete', () => {
        it('status 200', (done) => {
            var headers = { 'Content-Type': 'application/json' };
            var bici = '{ "id": 10, "color": "rojo", "modelo": "urbana", "lat":-25251465,"lng":-48756568964}';
            request.delete({
                    headers: headers,
                    url: 'http://localhost:5000/api/bicicletas/delete',
                    body: bici.id
                }, function(err, response, body) {
                    expect(response.statusCode).toBe(200);
                    done();

                }

            )
        })
    })
    describe('/UPDATE BICICLETAS /update', () => {
        it('status 200', (done) => {
            var headers = { 'Content-Type': 'application/json' };
            var bici = '{ "id": 10, "color": "rojo", "modelo": "urbana", "lat":-25251465,"lng":-48756568964}';
            request.post({
                    headers: headers,
                    url: 'http://localhost:5000/api/bicicletas/update',
                    body: bici.id
                }, function(err, response, body) {
                    expect(response.statusCode).toBe(200);
                    done();

                }

            )
        })
    })*/