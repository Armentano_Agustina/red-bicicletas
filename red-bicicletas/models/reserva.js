var mongoose = require('mongoose');
var moment = require('moment'); //libreria que trabaja con fechas y horas
var Schema = mongoose.Schema;

var ReservaSchema = new Schema({
    desde: Date,
    hasta: Date,
    Bicicleta: { type: mongoose.Schema.Types.ObjectId, ref: 'Bicicleta' },
    Usuario: { type: mongoose.Schema.Types.ObjectId, ref: 'Usuario' }
});

ReservaSchema.method.diasDeReserva = function() {
    return moment(this.hasta).diff(moment(this.desde), 'days') + 1;
    /*El método diff te permite hallar la diferencia en cualquier unidad y la sintáxis es
    moment.diff(moment, 'intervalo')*/
};

module.exports = mongoose.model('Reserva', ReservaSchema);