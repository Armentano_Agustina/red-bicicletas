var mongoose = require('mongoose');
var Reserva = require('./reserva');
var Schema = mongoose.Schema;

/*var ValidateEmail = function(email) {
    var re = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i; //expresion regulada
    return re.test(email) //El método test() ejecuta la búsqueda de 
        //una ocurrencia entre una expresión regular y una cadena especificada. Devuelve true o false.
}*/
var UsuarioSchema = new Schema({
    nombre: {
        type: String,
        required: [true, "El nombre es requerido"],
        trim: true
    }

})

UsuarioSchema.methods.reservar = function(biciId, desde, hasta, cb) {
    var reserva = new Reserva({ usuario: this.id, biciId: biciId, desde: desde, hasta: hasta });
    console.log(reserva);
    reserva.save(cb);
}


module.exports = mongoose.model('Usuario', UsuarioSchema);