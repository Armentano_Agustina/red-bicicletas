var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var BicicletaSchema = new Schema({
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number],
        index: { type: '2dsphere', sparse: true }
    }
});

BicicletaSchema.statics.createInstance = function(color, modelo, ubicacion) {
    return new this({
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

BicicletaSchema.methods.toString = function() {
    return 'id: ' + this._id + 'color: ' + this.color;
};

BicicletaSchema.statics.allBicis = function(cb) {
    return this.find({}, cb)
};

BicicletaSchema.statics.add = function(aBici, cb) {
    this.create(aBici, cb);
};
BicicletaSchema.statics.findByCode = function(id, cb) {
    return this.findOne({ _id: id }, cb);
};

BicicletaSchema.statics.removeByCode = function(id, cb) {
    this.deleteOne({ _id: id }, cb);
}


module.exports = mongoose.model('Bicicleta', BicicletaSchema);