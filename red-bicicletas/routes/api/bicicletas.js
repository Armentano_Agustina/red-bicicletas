var express = require('express');
var router = express.Router();
var bicicletaControllersApi = require('../../controllers/api/BicicletaControllersApi');

router.get('/', bicicletaControllersApi.bicicleta_list);
router.post('/create', bicicletaControllersApi.bicicleta_create);
router.delete('/delete', bicicletaControllersApi.bicicleta_delete);
router.put('/:id/update', bicicletaControllersApi.bicicleta_update);

module.exports = router;