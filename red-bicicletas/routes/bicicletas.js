var express = require('express');
var router = express.Router();
var bicicletaController = require('../controllers/bicicleta');

//Lista de bicicletas
router.get('/', bicicletaController.bicicleta_list);
//Crear bicicletas
router.get('/create', bicicletaController.bicicleta_create_get);
router.post('/create', bicicletaController.bicicleta_create_post);
//Actualizar Bicicletas
router.get('/:id/update', bicicletaController.bicicleta_update_get);
router.post('/:id/update', bicicletaController.bicicleta_update_post);
//Eliminar Bicicletas
router.post('/:id/delete', bicicletaController.bicicleta_delete_post);

module.exports = router;