require('../app');
var Bicicleta = require('../models/bicicleta');

console.log('con/bic Bicicleta=', Bicicleta);

exports.bicicleta_list = function(req, res) {
    let bicicletas = Bicicleta.find({});

    res.render('./bicicletas/index', bicicletas);
};

exports.bicicleta_create_get = function(req, res) {

    res.render('bicicletas/create');
}


exports.bicicleta_create_post = function(req, res) {

    var bici = new Bicicleta();
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bici, function(err, newBici) {
        if (err) console.log(err);
    })
    res.redirect('/bicicletas');
}


exports.bicicleta_update_get = function(req, res) {
    var bici = Bicicleta.findById(req.params._id);

    res.render('bicicletas/update', { bici });
}

exports.bicicleta_update_post = function(req, res) {

    var bici = Bicicleta.findById(req.params._id);

    bici._id = req.body._id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];

    res.redirect('/bicicletas');
}

exports.bicicleta_delete_post = function(req, res) {
    Bicicleta.removeById(req.body._id);
    res.redirect('/bicicletas'); //Muestra la lista de bicicletas
}