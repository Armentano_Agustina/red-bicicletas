var Bicicleta = require('../../models/bicicleta');
require('mongoose');
require('../../app')


exports.bicicleta_list = function(req, res) {
    Bicicleta.find(function(err, bicicletas) {
        if (err) return console.log(err);
        console.log(bicicletas);
        res.send(bicicletas);
    })
}

exports.bicicleta_create = function(req, res) {
    const bici = new Bicicleta();
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = req.body.ubicacion

    bici.save(function(err) {
        if (err) throw err;

        console.log('bici creada con exito.');
    })

    res.status(200).json({
        bicicleta: bici
    });
}

exports.bicicleta_delete = function(req, res) {
    Bicicleta.deleteOne({ _id: req.body._id }, function(err) {
        if (err) console.log(err);
    })
    Bicicleta.find(function(err, bicicletas) {
        if (err) return console.log(err);
        res.send(bicicletas);
    })
}

exports.bicicleta_update = function(req, res) {
    Bicicleta.findByIdAndUpdate(req.params.id, { color: req.body.color, modelo: req.body.modelo, ubicacion: req.body.ubicacion },
        function(err, doc) {
            if (err) console.log(err)
        })
    Bicicleta.findById(req.params.id, function(err, bici_actualizada) {
        if (err) console.log(err)
        res.send(
            bici_actualizada
        )
    })

}