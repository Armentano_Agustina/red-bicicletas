var map = L.map('mapid').setView([-31.429980, -64.185773], 13); // le pasamos el "id" del "div" que va a contener al mapa, setee el mapa, lo ubique en esta coordenada

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { //agregar la capa de "tiles" o de mapa, que es el ".tilelayer" que acá especificamos
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

$.ajax({
    dataType: 'json',
    url: 'api/bicicletas',
    success: function(result) {
        console.log(result);
        result.bicicletas.forEach(function(bici) {
            L.marker(bici.ubicacion, { title: bici.id }).addTo(map);
        })
    }
})